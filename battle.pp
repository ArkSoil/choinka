unit battle;
{$mode objfpc}
interface
uses sysutils,crt,math,types,termGraphEXP,keyboard;



procedure fight(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;var plr:types.player;enm:types.enemy;lists:listA;spr:spriteA;moves:types.moveA);
procedure fadeout(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;t:byte);
procedure history(var his:types.log;msg:string);
function addStat(stat,add:byte):integer;
procedure message(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;msg:string);

implementation

procedure fight(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;var plr:types.player;enm:types.enemy;lists:listA;spr:spriteA;moves:types.moveA);
	var
		i,dmg:integer;
		his: types.log; 
		turnOver:Boolean = false;
		battleOver:Boolean = False;
		lvlOver:Boolean = false;
		itemState: string[8] = '--------';
		moveState: string[4] = '----';
		maxhp: integer;
		lvlup:list;
		totalStats: integer;
		str:byte = 0;
		vit:byte = 0;
		int:byte = 0;
		pos:byte = 1;
	begin
		//battle init
		randomize;
		maxhp:=enm.hp;
		//battle pre drawing
		fadeout(b,b2,bc,bc2,random(3));

		
		
		
		drawString(b,bc,54,2,'HP');//hp bar
				
		drawString(b,bc,4,19,'HP');// hp bar
		drawString(b,bc,4,21,'MP');//mp bar
			
		drawSpr(b,bc,50,7,spr[enm.spr]);
		drawFrame(b,bc,1,1,36,9);
		drawFrame(b,bc,1,8,32,19);
		drawFrame(b,bc,1,18,30,24);
		for i:=0 to 5 do his[i]:= ' ';
		
		while (battleOver=false) do
			begin
				while (turnOver=false) do
					begin
						drawFrame(b,bc,52,1,80,4);
						drawString(b,bc,57,1,enm.name);
						drawString(b,bc,80-Length(inttostr(enm.hp)+'/'+inttostr(maxhp)),3,inttostr(enm.hp)+'/'+inttostr(maxhp));
						drawBar(b,bc,56,2,23,enm.hp,maxhp,4);
					
						drawLineH(b,bc,6,20,23,' ');
						drawString(b,bc,30-Length(inttostr(plr.hp)+'/'+inttostr(plr.hpMax)),20,inttostr(plr.hp)+'/'+inttostr(plr.hpMax));
						drawBar(b,bc,6,19,23,plr.hp,plr.hpMax,4);
						drawLineH(b,bc,6,22,23,' ');
						drawString(b,bc,30-Length(inttostr(plr.mp)+'/'+inttostr(plr.mpMax)),22,inttostr(plr.mp)+'/'+inttostr(plr.mpMax));
						drawBar(b,bc,6,21,23,plr.mp,plr.mpMax,1);
						drawRect(b,bc,2,2,36,7);
						for i:=0 to 5 do
							drawString(b,bc,3,2+i,his[i]);
						
						case drawMenu(b,b2,bc,bc2,4,9,lists[2]) of
							1: 
								begin
									enm.hp-=plr.str*moves[0].atc div 10;
									history(his,plr.name+' -> '+enm.name+' '+inttostr(plr.str*moves[0].atc div 10));
									turnOver:=true;
								end;
							2: 
								begin
									if 			plr.mp<10  then moveState:='xxxx'
									else if plr.mp<50  then moveState:='-xxx'
									else if plr.mp<100 then moveState:='--xx'
									else if plr.mp<200 then moveState:='---x'
									else										moveState:='----';

									for i:=0 to 3 do
										if moves[i+1].cost>plr.mp then
											drawString(b,bc,5,10+i*2,'koszt: '+inttostr(moves[i+1].cost)+' MP',8)
										else
											drawString(b,bc,5,10+i*2,'koszt: '+inttostr(moves[i+1].cost)+' MP',1);
									case drawMenu(b,b2,bc,bc2,4,9,lists[3],moveState) of
										1:
											begin
												plr.hp+=plr.int*moves[1].atc div 10;
												if plr.hp > plr.hpMax then plr.hp:=plr.hpMax;
												plr.mp-=moves[1].cost;
												history(his,plr.name+' -> '+plr.name+' '+inttostr(plr.int*moves[1].atc div 10));
												turnOver:=true;
											end;
										2:
											begin
												enm.hp-=plr.int*moves[2].atc div 10;
												plr.mp-=moves[2].cost;
												history(his,plr.name+' -> '+enm.name+' '+inttostr(plr.int*moves[2].atc div 10));
												turnOver:=true;
											end;
										3:
											begin
												enm.hp-=plr.int*moves[3].atc div 10;
												plr.mp-=moves[3].cost;
												history(his,plr.name+' -> '+enm.name+' '+inttostr(plr.int*moves[3].atc div 10));
												turnOver:=true;
											end;
										4:
											begin
												enm.hp-=plr.int*moves[4].atc div 10;
												plr.mp-=moves[4].cost;
												history(his,plr.name+' -> '+enm.name+' '+inttostr(plr.int*moves[4].atc div 10));
												turnOver:=true;
											end;
									end;
								end;
							3: 
								begin
									if plr.healmp < 1 then itemState[1]:='x';
									if plr.healhp < 1 then itemState[2]:='x';
									drawString(b,bc,5,10,'x'+inttostr(plr.healmp));
									drawString(b,bc,5,12,'x'+inttostr(plr.healhp));
									case drawMenu(b,b2,bc,bc2,4,9,lists[4],itemState) of 
										1: 
											if plr.healmp > 0 then
												begin
													plr.healmp-=1;
													plr.mp+=plr.mpMax div 2;
													if plr.mp > plr.mpMax then plr.mp:=plr.mpMax;
														itemState[1]:='-';
												end;
										2: 
											if plr.healhp > 0 then
												begin
													plr.healhp-=1;
													plr.hp+=plr.hpMax div 2;
													if plr.hp > plr.hpMax then plr.hp:=plr.hpMax;
														itemState[2]:='-';
												end;
									end;
								end;
							4: turnOver:=true;
						end;	
					end;
				if enm.hp>0 then
					begin
						dmg:=enm.atc+random(enm.atc);
						plr.hp-=dmg;
						history(his,enm.name+' ->'+plr.name+' '+inttostr(dmg));
						turnOver:=false;
					end;
				if (enm.hp<=0) or (plr.hp<=0) then battleOver:=true;
			end;
			//sweep
			drawRect(b,bc,52,1,81,3,' ');
			if enm.hp<=0 then
				begin
					for i:=1 to spr[enm.spr].height-1 do 
						begin
							drawRect(b,bc,50,7+i-1,50+spr[enm.spr].width,10+spr[enm.spr].height,' ');	
							drawSpr(b,bc,50,7+i,spr[enm.spr],0,i);
							drawBuffer(b,b2,bc,bc2);
							delay(75);
						end;
						drawRect(b,bc,50,7,50+spr[enm.spr].width,10+spr[enm.spr].height,' ');	
					message(b,b2,bc,bc2,'Dostales '+inttostr(enm.xpDrop)+' EXP');
				end
			else
				message(b,b2,bc,bc2,'Zostales zabity');
			
			plr.mp+=plr.mpMax div 2;
			if plr.mp > plr.mpMax then plr.mp:=plr.mpMax;

			
			//lvlup screen
			if (plr.lvl<245) and (plr.hp>0) then
				begin
					plr.xp+=enm.xpDrop;
					if plr.xp>=plr.xpMax then
						begin
							totalStats:=plr.xp div plr.xpMax*3;

							plr.lvl+=plr.xp div plr.xpMax;
							plr.xp-=(plr.xp div plr.xpMax) * plr.xpMax;
							
							plr.xpMax:=round(power(plr.lvl,2)*0.625)+10;
							drawFrame(b,bc,15,6,65,15);
							drawRect(b,bc,16,7,64,13);
							drawString(b,bc,19,7,'Nowy poziom! Dodaj '+inttostr(totalStats)+' punkty/ow do statystyk');
							drawString(b,bc,17,9,'VIT');
							drawBar(b,bc,20,9,40,plr.vit,255,4);

							drawString(b,bc,17,11,'STR');
							drawBar(b,bc,20,11,40,plr.str,255,14);

							drawString(b,bc,17,13,'INT');
							drawBar(b,bc,20,13,40,plr.int,255,1);

							drawBuffer(b,b2,bc,bc2);
							lvlup.size:=3;
							while(lvlOver=false) do
								begin
									lvlup.field[0]:='+'+inttostr(vit);
									lvlup.field[1]:='+'+inttostr(str);
									lvlup.field[2]:='+'+inttostr(int); 
									case drawMenu(b,b2,bc,bc2,60,9,lvlup,'---',pos) of
										0:
											begin
												totalStats+=vit+str+int;
												vit:=0;int:=0;str:=0;
											end;
										1:
											if (totalStats>0) and (plr.vit+vit<255) then
												begin
													vit+=1;
													totalStats-=1;
													pos:=1;
												end
											else if (totalStats<=0) then lvlOver:=true;
										2:
											if (totalStats>0) and (plr.str+str<255) then
												begin
													str+=1;
													totalStats-=1;
													pos:=2;
												end
											else if (totalStats<=0) then lvlOver:=true;
										3:
											if (totalStats>0) and (plr.int+int<255) then
												begin
													int+=1;
													totalStats-=1;
													pos:=3;
												end
											else if (totalStats<=0) then lvlOver:=true;
									end;
								end;
							plr.hpMax+=addStat(plr.vit,vit);
							plr.mpMax+=addStat(plr.int,int);
							plr.hp:=plr.hpMax;
							plr.mp:=plr.mpMax;
							plr.vit+=vit;
							plr.str+=str;
							plr.int+=int;
						end;
				end;
			
	end;

procedure fadeout(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer ;t:byte);
	var
		i,j,z,x: integer;
		arr: array[0..1919] of word;
	begin
		randomize;
		case t of
			0:
	 			for i:=0 to 23 do
			 		begin
			 			j:=0;
						while(j<=80) do
							begin
								drawChar(b,bc,j,i,' ');
								j+=2;
							end;
						j:=1;
						while(j<80) do
							begin
								drawChar(b,bc,j,23-i,' ');
								j+=2;
							end;
							drawBuffer(b,b2,bc,bc2);
							delay(50);   
					end;
			1:begin
				for i:=0 to 1919 do 
					arr[i]:=i;
				x:=0;
				for i:=0 to 1919 do
					begin
						j:=random(1920-x);
						drawChar(b,bc,arr[j] mod 80+1 ,arr[j] div 80,' ');
						x+=1;
						for z:=j to 1920-x do arr[z]:=arr[z+1];
						drawBuffer(b,b2,bc,bc2);
						delay(1);
						end;
					end;
				2:
					for i:=0 to 79 do
			 		begin
			 			j:=0;
						while(j<24) do
							begin
								drawChar(b,bc,i+1,j,' ');
								j+=2;
							end;
						j:=1;
						while(j<24) do
							begin
								drawChar(b,bc,80-i,j,' ');
								j+=2;
							end;
							drawBuffer(b,b2,bc,bc2);
							delay(15);   
					end;
				3:
					begin
					j:=1;
					z:=1;
					for i:=0 to 11 do
						begin
							while(j<23-i) do
								begin
									drawChar(b,bc,z,j,' ');
									drawBuffer(b,b2,bc,bc2);
									j+=1;
									delay(4);
								end;
							while(z<80-i) do
								begin
									drawChar(b,bc,z,j,' ');
									drawBuffer(b,b2,bc,bc2);
									z+=1;
									delay(2);
								end;
							while(j>1+i) do
								begin
									drawChar(b,bc,z,j,' ');
									drawBuffer(b,b2,bc,bc2);
									j-=1;
									delay(4);
								end;
							while(z>1+i+1) do
								begin
									drawChar(b,bc,z,j,' ');
									drawBuffer(b,b2,bc,bc2);
									z-=1;
									delay(2);
								end;
						end;
						end;
		end;
	end;
//
procedure history(var his:types.log;msg:string);
	var
		i:byte;
	begin
		for i:=0 to 4 do
			his[i]:=his[i+1];
		his[5]:=msg;
	end;


function addStat(stat,add:byte):integer;
	var 
		i:byte;
		x:integer = 0;
	begin
		if (add>0) then
			for i:=stat+1 to stat+add do
				x+=round(sin(i* 0.015625)*5  + 5);
		addStat:=x;
	end;

procedure message(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;msg:string);
	begin
		drawFrame(b,bc,(80-length(msg)) div 2-1,11,(80-length(msg)) div 2+length(msg),14);
		drawString(b,bc,(80-length(msg)) div 2+1,12,msg);
		drawBuffer(b,b2,bc,bc2);
		getKeyEvent;
	end;


end.