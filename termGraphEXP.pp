
unit termGraphEXP;
{$mode objfpc} 
interface
uses types,keyboard,crt,math;

procedure drawChar   (var b:types.charBuffer;var bc:types.colorBuffer; x,y:Integer;c:char;color:byte = 7);                                                            //draw char
procedure drawString (var b:types.charBuffer;var bc:types.colorBuffer; x,y:Integer;s:string;color:byte = 7);                                                          //draw string
procedure drawLineH  (var b:types.charBuffer;var bc:types.colorBuffer; x,y,width:integer;c:char = ' ';color:byte = 7);                                                //draw horizontal line
procedure drawLineV  (var b:types.charBuffer;var bc:types.colorBuffer; x,y,height:integer;c:char = ' ';color:byte = 7);                                               //draw vertical line
procedure drawLineDL (var b:types.charBuffer;var bc:types.colorBuffer; x,y,size:integer;c:char = ' ';color:byte = 7);                                                 //draw diagonal line (left)
procedure drawLineDR (var b:types.charBuffer;var bc:types.colorBuffer; x,y,size:integer;c:char = ' ';color:byte = 7);                                                 //draw diagonal line (right)
procedure drawRect   (var b:types.charBuffer;var bc:types.colorBuffer; x1,y1,x2,y2:integer;c:char = ' ';color:byte = 7);                                              //draw rectangle
procedure drawFrame  (var b:types.charBuffer;var bc:types.colorBuffer; x1,y1,x2,y2:integer;color:byte = 7);                                                           //draw frame
procedure drawBar    (var b:types.charBuffer;var bc:types.colorBuffer; x,y,width,currentValue,maxValue:integer;color:byte);                                           //draw proggres bar
function  drawMenu   (var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer; x,y:integer; menu:list;  state:string = '--------'; pos:byte = 1):byte;                 //draw menu
procedure drawSpr    (var b:types.charBuffer;var bc:types.colorBuffer;x,y:byte;spr:types.sprite;trimR:byte = 0;trimD:byte = 0;trimL:byte = 0;trimU:byte = 0);         //draw sprite
procedure draw3dView (var b:types.charBuffer;var bc:types.colorBuffer ;x,y:byte;vision:types.visionBuffer;events:types.eventM;eventState:string;spr:spriteA;d:byte);  //draws 3d view
procedure drawMinimap(var b:types.charBuffer;var bc:types.colorBuffer ;events:eventM;eventState:string;map1:types.map;x,y,x1,y1,x2,y2,d:byte);                        //draw minimap
procedure drawCompass(var b:types.charBuffer;var bc:types.colorBuffer; x,y:Integer;d:byte);                                                                           //draw compass
procedure drawBuffer (var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer);                                                                                        //draw charbuffer
procedure drawText   (var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;x,y:byte;msg:string;d:byte = 10);                                                        //draw string one by one with delay
implementation

procedure drawChar(var b:types.charBuffer;var bc:types.colorBuffer ;x,y:Integer;c:char;color:byte = 7);
  begin
    b[x][y]:=c;
    bc[x][y]:=color;
  end;
//
procedure drawString(var b:types.charBuffer;var bc:types.colorBuffer ;x,y:Integer;s:string;color:Byte = 7);
  var
    i: Integer;
  begin
    for i:=1 to Length(s) do
      begin
        b[x+i-2][y]:=s[i];
        bc[x+i-2][y]:=color;
      end;
  end;
//
procedure drawLineH(var b:types.charBuffer;var bc:types.colorBuffer ;x,y,width:integer;c:char = ' ';color:byte = 7);
  var
    i: integer;
  begin
    for i:=0 to width-1 do
    begin
      b[x+i][y]:=c;
      bc[x+i][y]:=color;
    end; 
  end;
//
procedure drawLineV(var b:types.charBuffer;var bc:types.colorBuffer ;x,y,height:integer;c:char = ' ';color:byte = 7);
    var
    i: integer;
  begin
    for i:=0 to height-1 do
    begin
      b[x][y+i]:=c;
      bc[x][y+i]:=color;
    end; 
  end;
//
procedure drawLineDL(var b:types.charBuffer;var bc:types.colorBuffer ;x,y,size:integer;c:char = ' ';color:byte = 7);
  var
		i:integer;
  begin
		for i:=0 to size-1 do
			begin
			b[x+i*2][y+i]:=c;
      bc[x+i*2][y+i]:=color;
			end;
  end;
//
procedure drawLineDR(var b:types.charBuffer;var bc:types.colorBuffer ;x,y,size:integer;c:char = ' ';color:byte = 7);
  var
		i:integer;
  begin
		for i:=0 to size-1 do
			begin
			b[x-i*2][y+i]:=c;
      bc[x-i*2][y+i]:=color;
			end;
  end;
//
procedure drawRect(var b:types.charBuffer;var bc:types.colorBuffer ;x1,y1,x2,y2:integer;c:char = ' ';color:byte = 7);
  var
    i: integer;
  begin
    for i:=0 to y2-y1 do
      begin
        drawLineH(b,bc,x1,y1+i,x2-x1,c,color);
      end; 
  end;
//
procedure drawFrame(var b:types.charBuffer;var bc:types.colorBuffer ;x1,y1,x2,y2:integer;color:byte = 7);
  begin
    drawLineH(b,bc,x1,y1,x2-x1,'-',color);
    drawLineH(b,bc,x1,y2-1,x2-x1,'-',color);
    drawLineV(b,bc,x1,y1,y2-y1,'|',color);
    drawLineV(b,bc,x2,y1,y2-y1,'|',color);
    b[x1][y1]:='+';b[x1][y2-1]:='+';b[x2][y1]:='+';b[x2][y2-1]:='+';
    bc[x1][y1]:=color;bc[x1][y2-1]:=color;bc[x2][y1]:=color;bc[x2][y2-1]:=color;
  end;
//
procedure drawBar(var b:types.charBuffer;var bc:types.colorBuffer;x,y,width,currentValue,maxValue:integer;color:byte);
  begin
    drawLineH(b,bc,x,y,width,']',8);
    drawLineH(b,bc,x,y,width*currentValue div maxValue,']',color);
  end;
//
function drawMenu(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;x,y:integer; menu:types.list; state:string = '--------';pos:byte = 1):byte;
  var
    K:TKeyEvent;
    i,max:byte;
  begin
    while keypressed do k:=getkeyevent; // clearing keybuffer
    repeat
        while state[pos] = 'x' do
          pos+=1;
        //draw start
        for i:=0 to menu.size-1 do
          if pos-1=i then
            drawString(b,bc,x,y+i*2,'>'+menu.field[i],4)
          else
            if state[i+1]<>'x' then
              drawString(b,bc,x,y+i*2,' '+menu.field[i])
            else
              drawString(b,bc,x,y+i*2,' '+menu.field[i],8);
        drawBuffer(b,b2,bc,bc2);
        //draw end
        K:=getKeyEvent;
        K:=TranslateKeyEvent(K);
        Case KeyEventToString(K) of
          'x': pos:=0;
          'Up': repeat
                      pos-=1;
                      if pos<1 then
                        pos:=menu.size
                until(state[pos]<>'x');
          'Down': repeat
                    pos+=1;
                    if pos>menu.size then
                      pos:=1;
                  until(state[pos]<>'x');
        end;
        
        
      while keypressed do k:=getkeyevent; // clearing keybuffer
    until((KeyEventToString(K)='z') or (KeyEventToString(K)='x'));
    //clean
    max := 0;
    for i:=0 to menu.size-1 do
      if max < length(menu.field[i]) then max:=length(menu.field[i]);
    drawRect(b,bc,x-1,y,x+max,y+(menu.size-1)*2,' ');
    drawMenu:=pos;
  end;
//
procedure drawSpr(var b:types.charBuffer;var bc:types.colorBuffer;x,y:byte;spr:types.sprite;trimR:byte = 0;trimD:byte = 0;trimL:byte = 0;trimU:byte = 0);
  var
    i,j:byte;
  begin
    for i:=trimL to spr.width-1-trimR do
      for j:=trimU to spr.height-1-trimD do
        if spr.clrMap[i,j]<>255 then
          drawChar(b,bc,x+i-trimL,y+j-trimU,spr.scr[i,j],spr.clrMap[i,j]);
  end;
//
procedure drawMinimap(var b:types.charBuffer;var bc:types.colorBuffer ;events:eventM;eventState:string;map1:types.map;x,y,x1,y1,x2,y2,d:byte);
  var
    i,j,z:integer;
  begin
  for j:=0 to y2-y1 do
				for i:=0 to x2-x1 do
        begin
          if (i+x-(x2-x1+3) div 2< 0) or
             (j+y-(y2-y1+3) div 2< 0) or
             (i+x-(x2-x1+3) div 2>= map1.width) or
             (j+y-(y2-y1+3) div 2>= map1.height) then
            drawChar(b,bc,i+x1,j+y1,' ',6)
          else
          begin
          if (map1.field[j+y-(y2-y1+3) div 2][i+x-(x2-x1+3) div 2-1]<>'0') and 
                  (map1.field[j+y-(y2-y1+3) div 2][i+x-(x2-x1+3) div 2+1]<>'0') and 
                  (map1.field[j+y-(y2-y1+3) div 2-1][i+x-(x2-x1+3) div 2]<>'0') and 
                  (map1.field[j+y-(y2-y1+3) div 2-1][i+x-(x2-x1+3) div 2-1]<>'0') and 
                  (map1.field[j+y-(y2-y1+3) div 2-1][i+x-(x2-x1+3) div 2+1]<>'0') and 
                  (map1.field[j+y-(y2-y1+3) div 2+1][i+x-(x2-x1+3) div 2]<>'0') and 
                  (map1.field[j+y-(y2-y1+3) div 2+1][i+x-(x2-x1+3) div 2-1]<>'0') and 
                  (map1.field[j+y-(y2-y1+3) div 2+1][i+x-(x2-x1+3) div 2+1]<>'0') then
            drawChar(b,bc,i+x1,j+y1,' ',6)
          else if (map1.field[j+y-(y2-y1+3) div 2][i+x-(x2-x1+3) div 2-1]='1') and 
                  (map1.field[j+y-(y2-y1+3) div 2][i+x-(x2-x1+3) div 2+1]='1') and 
                 ((map1.field[j+y-(y2-y1+3) div 2-1][i+x-(x2-x1+3) div 2]='0') or 
                  (map1.field[j+y-(y2-y1+3) div 2+1][i+x-(x2-x1+3) div 2]='0')) then
            drawChar(b,bc,i+x1,j+y1,'-',6)
          else if (map1.field[j+y-(y2-y1+3) div 2-1][i+x-(x2-x1+3) div 2]='1') and 
                  (map1.field[j+y-(y2-y1+3) div 2+1][i+x-(x2-x1+3) div 2]='1') and
                 ((map1.field[j+y-(y2-y1+3) div 2][i+x-(x2-x1+3) div 2-1]='0') or 
                  (map1.field[j+y-(y2-y1+3) div 2][i+x-(x2-x1+3) div 2+1]='0')) then
            drawChar(b,bc,i+x1,j+y1,'|',6)
          else 
            drawChar(b,bc,i+x1,j+y1,'+',6);
          if map1.field[j+y-(y2-y1+3) div 2][i+x-(x2-x1+3) div 2] = '0'  then
            drawChar(b,bc,i+x1,j+y1,' ',6);
          end;
          
          //draw events
          for z:=0 to 9 do
              if (i+x-(x2-x1+3) div 2=events[z].x) and (j+y-(y2-y1+3) div 2=events[z].y) and (eventState[z+1]<>'x') then
                drawChar(b,bc,x1+i,y1+j,'X',4);

        end;
        case d of
          0: drawChar(b,bc,x1+(x2-x1+3) div 2,y1+(y2-y1+3) div 2,'^',4);
          1: drawChar(b,bc,x1+(x2-x1+3) div 2,y1+(y2-y1+3) div 2,'>',4);
          2: drawChar(b,bc,x1+(x2-x1+3) div 2,y1+(y2-y1+3) div 2,'v',4);
          3: drawChar(b,bc,x1+(x2-x1+3) div 2,y1+(y2-y1+3) div 2,'<',4); 
        end;
  end;
//
procedure drawCompass(var b:types.charBuffer;var bc:types.colorBuffer ;x,y:Integer;d:byte);
  begin
    drawChar(b,bc,x+6,y+1,'^');
    drawChar(b,bc,x+6,y+3,'v');
    drawChar(b,bc,x+4,y+2,'<');
    drawChar(b,bc,x+8,y+2,'>');
    case d of
      0:begin
          drawChar(b,bc,x+6,y,'N',4);
          drawChar(b,bc,x+6,y+4,'S');
          drawChar(b,bc,x+2,y+2,'W');
          drawChar(b,bc,x+10,y+2,'E');
          drawString(b,bc,x+3,y+5,'Polnoc');
        end;
      1:begin
          drawChar(b,bc,x+6,y,'E');
          drawChar(b,bc,x+6,y+4,'W');
          drawChar(b,bc,x+2,y+2,'N',4);
          drawChar(b,bc,x+10,y+2,'S');
          drawString(b,bc,x+3,y+5,'Wschod');
        end;

      2:begin
          drawChar(b,bc,x+6,y,'S');
          drawChar(b,bc,x+6,y+4,'N',4);
          drawChar(b,bc,x+2,y+2,'E');
          drawChar(b,bc,x+10,y+2,'W');
          drawString(b,bc,x+3,y+5,'Poludnie');
        end;

      3:begin
          drawChar(b,bc,x+6,y,'W');
          drawChar(b,bc,x+6,y+4,'E');
          drawChar(b,bc,x+2,y+2,'S');
          drawChar(b,bc,x+10,y+2,'N',4);
          drawString(b,bc,x+3,y+5,'Zachod');
        end;

    end;
  end;
//
procedure draw3dView(var b:types.charBuffer;var bc:types.colorBuffer ;x,y:byte;vision:types.visionBuffer;events:types.eventM;eventState:string;spr:spriteA;d:byte);
  var
    i:integer;
  begin
    //const lines
    drawLineH(b,bc,12,7,18,'-',6);// front
    drawLineH(b,bc,12,16,18,'-',6);
    drawLineH(b,bc,19,10,3,'-',6);//back
    drawLineH(b,bc,19,13,3,'-',6);
    
    //backerrow
    if vision[7] <> '0' then //0
      begin
        drawLineH(b,bc,19,11,3,'-',2);
        drawLineH(b,bc,19,12,3,'-',2);
        drawLineV(b,bc,18,11,2,'|',6);
        drawLineV(b,bc,22,11,2,'|',6);
      end;
    if vision[8] <> '0' then //-1
    begin
      drawLineH(b,bc,7,11,11,'-',2);
      drawLineH(b,bc,7,12,11,'-',2);
      drawLineV(b,bc,6,11,2,'|',6);
      drawLineV(b,bc,18,11,2,'|',6);
    end;
    if vision[9] <> '0' then //+1
      begin
      drawLineH(b,bc,23,11,11,'-',2);
      drawLineH(b,bc,23,12,11,'-',2);
      drawLineV(b,bc,22,11,2,'|',6);
      drawLineV(b,bc,34,11,2,'|',6);
      end;
    if vision[10] <> '0' then//-2
      begin
      drawLineH(b,bc,2,11,4,'-',2);
      drawLineH(b,bc,2,12,4,'-',2);
      drawLineV(b,bc,6,11,2,'|',6);
      end;
    if vision[11] <> '0' then//+2
      begin
        drawLineH(b,bc,35,11,4,'-',2);
        drawLineH(b,bc,35,12,4,'-',2);
        drawLineV(b,bc,34,11,2,'|',6);
      end;

    //backrow
    if vision[3] = '0' then //-1
      begin
        //draw -1 empty
        drawLineH(b,bc,7,10,12,'-',6);
        drawLineH(b,bc,7,13,12,'-',6);
        if vision[5] = '0' then //-2
          begin
            //draw -2 empty
            drawLineH(b,bc,2,10,7,'-',6);
            drawLineH(b,bc,2,13,7,'-',6);
          end
        else
          begin
            //draw -2 full
            drawLineV(b,bc,6,11,2,'|',6);
            drawLineDL(b,bc,2,8,3,'\',6);
            drawLineDR(b,bc,6,13,3,'/',6);
            for i:=0 to 1 do
              begin
                drawLineV(b,bc,2+i*2,9+i,6-i*2,'|',2);
                drawLineV(b,bc,3+i*2,9+i,(6-i*2) div 2,'\',2);
                drawLineV(b,bc,3+i*2,9+i+(6-i*2) div 2,(6-i*2) div 2,'/',2);
              end;
          end;
      end

    else
      begin
        //draw -1 full
        drawLineDL(b,bc,14,8,3,'\',6);
        drawLineDR(b,bc,18,13,3,'/',6);
        drawLineV(b,bc,18,11,2,'|',6);
        for i:=0 to 2 do
          begin
            drawLineV(b,bc,12+i*2,8+i,8-i*2,'|',2);
            drawLineV(b,bc,13+i*2,8+i,(8-i*2) div 2,'\',2);
            drawLineV(b,bc,13+i*2,8+i+(8-i*2) div 2,(8-i*2) div 2,'/',2);
          end;
        drawRect(b,bc,2,8,12,15,'-',2);   
      end;

    if vision[4] = '0' then //+1
      begin
        //draw +1 empty
        drawLineH(b,bc,22,10,12,'-',6);
        drawLineH(b,bc,22,13,12,'-',6);
        if vision[6] = '0' then //+2
          begin
            //draw +2 empty
            drawLineH(b,bc,32,10,7,'-',6);
            drawLineH(b,bc,32,13,7,'-',6);
          end
        else
          begin
            //draw +2 full
            drawLineV(b,bc,34,11,2,'|',6);
            drawLineDL(b,bc,34,13,3,'\',6);
            drawLineDR(b,bc,38,8,3,'/',6);
            for i:=0 to 1 do
              begin
                drawLineV(b,bc,38-i*2,9+i,6-i*2,'|',2);
                drawLineV(b,bc,37-i*2,9+i,(6-i*2) div 2,'/',2);
                drawLineV(b,bc,37-i*2,9+i+(6-i*2) div 2,(6-i*2) div 2,'\',2);
              end;
          end;
      end
    else
      begin
        //draw +1 full
        drawLineDL(b,bc,22,13,3,'\',6);
        drawLineDR(b,bc,26,8,3,'/',6);
        drawLineV(b,bc,22,11,2,'|',6);
        for i:=0 to 2 do
          begin
            drawLineV(b,bc,28-i*2,8+i,8-i*2,'|',2);
            drawLineV(b,bc,27-i*2,8+i,(8-i*2) div 2,'/',2);
            drawLineV(b,bc,27-i*2,8+i+(8-i*2) div 2,(8-i*2) div 2,'\',2);
          end;
          drawRect(b,bc,29,8,39,15,'-',2);
      end;
    if vision[0] = '0' then //0
      begin
        //draw 0 empty
      end
    else
      begin
        //draw 0 full
        drawRect(b,bc,13,8,28,15,'-',2);
      end;

    //supplementary lines
    if (vision[1] <> '0') or (vision[3] <> '0') or (vision[0] <> '0') then
      drawLineV(b,bc,12,8,8,'|',6);
    if (vision[2] <> '0') or (vision[4] <> '0') or (vision[0] <> '0') then
      drawlineV(b,bc,28,8,8,'|',6);

    //back event drawing
    for i:=0 to 9 do
      if eventState[i+1]<>'x' then 
        begin
          case d of
            0:
              begin
                if y-1=events[i].y then
                  if      x=events[i].x   then drawSpr(b,bc,18,13,spr[7])
                  else if x+1=events[i].x then drawSpr(b,bc,29,13,spr[7])
                  else if x-1=events[i].x then drawSpr(b,bc,7,13,spr[7]);
              end;
            1:  
              begin
                if x+1=events[i].x then
                  if      y=events[i].y   then drawSpr(b,bc,18,13,spr[7])
                  else if y+1=events[i].y then drawSpr(b,bc,29,13,spr[7])
                  else if y-1=events[i].y then drawSpr(b,bc,7,13,spr[7]);
              end;
            2:
              begin
                if y+1=events[i].y then
                if      x=events[i].x   then drawSpr(b,bc,18,13,spr[7])
                else if x-1=events[i].x then drawSpr(b,bc,29,13,spr[7])
                else if x+1=events[i].x then drawSpr(b,bc,7,13,spr[7]);
              end;
            3:
              begin
                if x-1=events[i].x then
                if      y=events[i].y   then drawSpr(b,bc,18,13,spr[7])
                else if y-1=events[i].y then drawSpr(b,bc,29,13,spr[7])
                else if y+1=events[i].y then drawSpr(b,bc,7,13,spr[7]);
              end;
          end;
        end;

    //frontrow
    if vision[1] = '0' then //-1
      begin
        drawLineH(b,bc,2,7,10,'-',6);
        drawLineH(b,bc,2,16,10,'-',6);
      end
    else
      begin
        drawLineDL(b,bc,2,2,6,'\',6);
        drawLineDR(b,bc,12,16,6,'/',6);
        for i:=0 to 4 do
          begin
            drawLineV(b,bc,2+i*2,3+i,18-i*2,'|',2);
            drawLineV(b,bc,3+i*2,3+i,(18-i*2) div 2,'\',2);
            drawLineV(b,bc,3+i*2,3+i+(18-i*2) div 2,(18-i*2) div 2,'/',2);
          end;
      end;

    if vision[2] = '0' then //+1
      begin
       drawLineH(b,bc,30,7,9,'-',6);
        drawLineH(b,bc,30,16,9,'-',6);
      end
    else
      begin
        drawLineDL(b,bc,28,16,6,'\',6);
        drawLineDR(b,bc,38,2,6,'/',6);
        for i:=0 to 4 do
          begin
            drawLineV(b,bc,38-i*2,3+i,18-i*2,'|',2);
            drawLineV(b,bc,37-i*2,3+i,(18-i*2) div 2,'/',2);
            drawLineV(b,bc,37-i*2,3+i+(18-i*2) div 2,(18-i*2) div 2,'\',2);
          end;
      end;
    //front event drawing
    for i:=0 to 9 do
      if eventState[i+1]<>'x' then 
        begin
          if (x=events[i].x) and (y=events[i].y) then drawSpr(b,bc,14,13,spr[4]); 
          case d of
            0:
              begin
                if y=events[i].y then
                  if      x+1=events[i].x then drawSpr(b,bc,31,13,spr[4],5)
                  else if x-1=events[i].x then drawSpr(b,bc,2,13,spr[4],0,0,5);
              end;
            1:  
              begin
                if x=events[i].x then
                  if      y+1=events[i].y then drawSpr(b,bc,31,13,spr[4],5)
                  else if y-1=events[i].y then drawSpr(b,bc,2,13,spr[4],0,0,5);
              end;
            2:
              begin
                if y=events[i].y then
                  if      x-1=events[i].x then drawSpr(b,bc,31,13,spr[4],5)
                  else if x+1=events[i].x then drawSpr(b,bc,2,13,spr[4],0,0,5);
              end;
            3:
              begin
                if x=events[i].x then
                  if y-1=events[i].y then drawSpr(b,bc,31,13,spr[4],5)
                  else if y+1=events[i].y then drawSpr(b,bc,2,13,spr[4],0,0,5);
              end;
          end;
        end;
  end;
//
procedure drawBuffer(var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer);
  var
    i,j:byte;
  begin
    for j:=1 to 23 do
      for i:=1 to 80 do
        if (b[i][j] <> b2[i][j]) or (bc[i][j] <> bc2[i][j]) then
          begin
            gotoxy(i,j);
            textcolor(bc[i][j]);
            write(b[i][j]);
          end;
    b2:=b;
    bc2:=bc;
    gotoxy(1,24); //cursor rest position
  end;
//
procedure drawText (var b,b2:types.charBuffer;var bc,bc2:types.colorBuffer;x,y:byte;msg:string;d:byte = 10);
  var 
    i:byte;
  begin
    for i:=0 to length(msg)-1 do
      begin
        drawChar(b,bc,x+i,y,msg[i+1]);
        drawBuffer(b,b2,bc,bc2);
        delay(d);
      end;
  end;
end.