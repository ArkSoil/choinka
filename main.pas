program main;
{$mode objfpc} 
uses sysutils,crt,termGraphEXP,types,battle,math,keyboard;


var
  b:   types.charbuffer;          // char buffer
  b2:  types.charBuffer;          // char buffer previous state
  bc:  types.colorBuffer;         // color buffer
  bc2: types.colorBuffer;         // color buffer previous state
  dat: data;                      // game data
  infileD: file of types.data;    // load data
  outfileS: file of types.player; // save/load proggres
  filename: string[23];           // save file filename
  i,j:integer;                    // inc variables
  vision: types.visionBuffer;     // player fov
  plr: types.player;              // player
  K: TKeyEvent;                   // key variable
  quit: boolean = false;          // game state flag
  menu: boolean = true;           // menu state flag

begin
  //game initialization start
  randomize;
  clrscr();
  InitKeyBoard;
  cursoroff;
  //loading files start
  Assign(infileD, 'data.dat');
  Reset(infileD);
  while not eof(infileD) do
    read(infileD, dat);
  close(infileD);
  //main menu
  repeat
  FillChar(b,80*23,' '); //zeroing char buffer
  FillChar(b2,80*23,'*'); // filling seccharbuffer
    drawSpr(b,bc,5,5,dat.sprites[8]);
    drawSpr(b,bc,50,10,dat.sprites[0]);
    drawString(b,bc,6,19,'z/enter - zatwierdz');
    drawString(b,bc,6,20,'x - anuluj/menu');
    drawString(b,bc,6,21,'strzalki - poruszanie');
    drawString(b,bc,6,22,'PgUp/PgDn - strafe');
    while menu=true do
      begin
      case drawMenu(b,b2,bc,bc2,6,11,dat.lists[0]) of
        1:
          begin
            drawString(b,bc,20,10,'Podaj imie gracza:');
            drawBuffer(b,b2,bc,bc2);
            gotoxy(20,11);
            readln(plr.name);
            plr.map:=0;
            plr.x:=dat.maps[plr.map].startX;
            plr.y:=dat.maps[plr.map].starty;
            plr.xBuffer:=dat.maps[plr.map].startX;
            plr.yBuffer:=dat.maps[plr.map].starty;
            plr.str:=10;
            plr.vit:=10;
            plr.int:=10;
            plr.hpMax:=54;
            plr.mpMax:=54;
            plr.hp:=54;
            plr.mp:=54;
            plr.lvl:=1;
            plr.xp:=0;
            plr.xpMax:=round(power(plr.lvl,2)*0.625)+10;
            plr.healhp:=3;
            plr.healmp:=1;
            plr.d:=dat.maps[plr.map].startD;
            plr.itemState := '--------';
            plr.eventState:= '----------';
            FillChar(b2,80*23,'*');
            //story cinematic
            FillChar(b,80*23,' '); //zeroing char buffer
            FillChar(b2,80*23,'*'); // zeroing seccharbuffer

            drawText(b,b2,bc,bc2,1,12,'Jest rok 20XX',20);
            while keypressed do getkeyevent; // clearing keybuffer
            getkeyevent;
            drawText(b,b2,bc,bc2,1,12,'Duch swiat zostal przejety i podzielony na trzy czesci przez zle sily',20);
            while keypressed do k:=getkeyevent; // clearing keybuffer
            getkeyevent;
            drawText(b,b2,bc,bc2,1,12,plr.name+' przyszlosc swiat jest w twoich rekach                               ',20);
            while keypressed do k:=getkeyevent; // clearing keybuffer
            getkeyevent;
            drawText(b,b2,bc,bc2,1,12,'Wyrusz w glab wielkiej choinki i uratuj swieta!                      ',20);
            while keypressed do k:=getkeyevent; // clearing keybuffer
            getkeyevent;
            menu:=false;
          end;
          2:
            begin
              drawString(b,bc,20,10,'Podaj imie gracza:');
              drawBuffer(b,b2,bc,bc2);
              gotoxy(20,11);
              readln(filename);
              if fileexists(filename+'.sav') then
                begin
                  Assign(outfileS, filename+'.sav');
                  Reset(outfileS);
                  while not eof(outfileS) do
                    read(outfileS, plr);
                  close(outfileS);
                  menu:=false;
                end
              else
                begin
                  drawString(b,bc,20,10,'Brak pliku zapisu!  ',4);
                  FillChar(b2,80*23,'*');
                end;
            end;
          3: 
            begin
              quit:=true;
              menu:=false;
            end;
        end;
      end;
    if quit=false then
    begin
      FillChar(b2,80*23,'*');
      repeat
        //game loop start
        FillChar(b,80*23,' '); //zeroing char buffer
        vision:=mapToVision(dat.maps[plr.map],plr.x,plr.y,plr.d);//get fov
        //frames
        drawFrame(b,bc,39,1,80,24);//misc
          drawFrame(b,bc,39,1,68,9);//bars
            drawString(b,bc,42,2,'HP');// hp bar
              drawString(b,bc,68-Length(inttostr(plr.hp)+'/'+inttostr(plr.hpMax)),3,inttostr(plr.hp)+'/'+inttostr(plr.hpMax));
                drawBar(b,bc,44,2,23,plr.hp,plr.hpMax,4);
            drawString(b,bc,42,4,'MP');//mp bar
              drawString(b,bc,68-Length(inttostr(plr.mp)+'/'+inttostr(plr.mpMax)),5,inttostr(plr.mp)+'/'+inttostr(plr.mpMax));
                drawBar(b,bc,44,4,23,plr.mp,plr.mpMax,1);
            drawString(b,bc,42,6,'XP');//xp bar
              drawString(b,bc,68-Length(inttostr(plr.xp)+'/'+inttostr(plr.xpMax)),7,inttostr(plr.xp)+'/'+inttostr(plr.xpMax));
                drawBar(b,bc,44,6,23,plr.xp,plr.xpMax,2);  

          drawFrame(b,bc,68,1,80,9);//compass
            drawCompass(b,bc,68,2,plr.d); //draw compass

          drawFrame(b,bc,39,8,59,14);//stats
            drawString(b,bc,42,9, 'LVL ' + inttostr(plr.lvl));
            drawString(b,bc,42,10,'VIT ' + inttostr(plr.vit));
            drawString(b,bc,42,11,'STR ' + inttostr(plr.str));
            drawString(b,bc,42,12,'INT ' + inttostr(plr.int));

          drawFrame(b,bc,39,8,59,24);//minimap
          drawMinimap(b,bc,dat.events[plr.map],plr.eventState,dat.maps[plr.map],plr.x,plr.y,60,9,79,22,plr.d);

        drawFrame(b,bc,1,1,39,23);//3d view frame
          draw3dView(b,bc,plr.x,plr.y,vision,dat.events[plr.map],plr.eventState,dat.sprites,plr.d);//draw 3dview
        //frames end
        drawChar(b,bc,1,23,'+');
        drawLineH(b,bc,2,23,37,'-');
        drawString(b,bc,3,23,concat('(' , inttostr(plr.x) , ', ' , inttostr(plr.y) , ')---' , inttostr(plr.d) , '---Pietro ', inttostr(plr.map+1)));
        
        //drawSpr(b,bc,14,13,dat.sprites[4]);
        //drawSpr(b,bc,18,13,dat.sprites[7]);
        //drawSpr(b,bc,31,13,dat.sprites[4],5);
        drawBuffer(b,b2,bc,bc2);

        //controlls start
            K:=getKeyEvent;
            K:=TranslateKeyEvent(K);
            Case KeyEventToString(K) of
              'Left': if plr.d=0 then plr.d:=3 else plr.d-=1;
              'Right': if plr.d=3 then plr.d:=0 else plr.d+=1;
              'Up': case plr.d of
                0: plr.yBuffer-=1;
                1: plr.xBuffer+=1;
                2: plr.yBuffer+=1;
                3: plr.xBuffer-=1;
                  end;
              'Down': case plr.d of
                0: plr.yBuffer+=1;
                1: plr.xBuffer-=1;
                2: plr.yBuffer-=1;
                3: plr.xBuffer+=1;
              end;
              'PgUp': case plr.d of
                0: plr.xBuffer-=1;
                1: plr.yBuffer-=1;
                2: plr.xBuffer+=1;
                3: plr.yBuffer+=1;
              end;
              'PgDn': case plr.d of
                0: plr.xBuffer+=1;
                1: plr.yBuffer+=1;
                2: plr.xBuffer-=1;
                3: plr.yBuffer-=1;
              end;
              'z':
                begin
                  for i:=0 to 9 do
                    if (plr.x=dat.events[plr.map][i].x) and (plr.y=dat.events[plr.map][i].y) and (plr.eventState[i+1]<>'x') then
                      begin
                        case dat.events[plr.map][i].eventType of
                          0:
                            begin
                              plr.eventState[i+1]:='x';
                              plr.healhp+=1;
                              message(b,b2,bc,bc2,'Dostales makowke');
                            end;
                          1:
                            begin
                              plr.eventState[i+1]:='x';
                              plr.healmp+=1;
                              message(b,b2,bc,bc2,'Dostales kompot ze sliwek');
                            end;
                          2: 
                            begin
                              plr.eventState[i+1]:='x';
                              fight(b,b2,bc,bc2,plr,dat.enemies[2+(4*plr.map)],dat.lists,dat.sprites,dat.moves);
                              case random(2) of
                                0:
                                  begin
                                    plr.healhp+=1;
                                    message(b,b2,bc,bc2,'Dostales makowke');
                                  end;
                                1: 
                                  begin
                                    plr.healmp+=1;
                                    message(b,b2,bc,bc2,'Dostales kompot ze sliwek');
                                  end;
                              end;
                            end;
                          3: 
                            begin
                              message(b,b2,bc,bc2,'Znalazles pierwszego straznika');
                              fight(b,b2,bc,bc2,plr,dat.enemies[3+(4*plr.map)],dat.lists,dat.sprites,dat.moves);
                              plr.map+=1;
                              plr.x       :=dat.maps[plr.map].startX;
                              plr.y       :=dat.maps[plr.map].startY;
                              plr.yBuffer := plr.x;
                              plr.xBuffer := plr.y;
                              plr.eventState:='----------';
                            end;
                          4:
                            begin
                              message(b,b2,bc,bc2,'Znalazles drugiego straznika');
                              fight(b,b2,bc,bc2,plr,dat.enemies[3+(4*plr.map)],dat.lists,dat.sprites,dat.moves);
                              plr.map+=1;
                              plr.x       :=dat.maps[plr.map].startX;
                              plr.y       :=dat.maps[plr.map].startY;
                              plr.yBuffer := plr.x;
                              plr.xBuffer := plr.y;
                              plr.eventState:='----------';
                            end;
                          5:
                            begin
                            message(b,b2,bc,bc2,'Znalazles ostatniego straznika');
                              fight(b,b2,bc,bc2,plr,dat.enemies[3+(4*plr.map)],dat.lists,dat.sprites,dat.moves);
                            message(b,b2,bc,bc2,'Gratulacje!!! Uratowales swieta');
                            plr.hp:=0;
                            end;
                        end;
                      end;
                end;
              'x': case drawMenu(b,b2,bc,bc2,42,15,dat.lists[1]) of
                1: begin
                    if plr.healmp < 1 then plr.itemState[1]:='x';
                    if plr.healhp < 1 then plr.itemState[2]:='x';
                    drawString(b,bc,43,16,'x'+inttostr(plr.healmp));
                    drawString(b,bc,43,18,'x'+inttostr(plr.healhp));
                    case drawMenu(b,b2,bc,bc2,42,15,dat.lists[4],plr.itemState) of 
                      1: if plr.healmp > 0 then
                          begin
                          plr.healmp-=1;
                          plr.mp+=plr.mpMax div 2;
                          if plr.mp > plr.mpMax then plr.mp:=plr.mpMax;
                          plr.itemState[1]:='-';
                          end;
                      2: if plr.healhp > 0 then
                        begin
                          plr.healhp-=1;
                          plr.hp+=plr.hpMax div 2;
                          if plr.hp > plr.hpMax then plr.hp:=plr.hpMax;
                          plr.itemState[2]:='-';
                        end;
                    end;
                  end;
                2: 
                  begin
                    Assign(outfileS,plr.name+'.sav');
                    Rewrite(outfileS);
                    write(outfileS, plr);
                    close(outfileS);
                  end;
                3: plr.hp:=0;
              end;
            end;

          
          if dat.maps[plr.map].field[plr.yBuffer][plr.xBuffer]='1' then
          begin
            plr.xBuffer:=plr.x;
            plr.yBuffer:=plr.y;
          end
        else
          begin
            if (random(100)<20) and (plr.x<>plr.xBuffer) and (plr.x<>plr.xBuffer) then
              fight(b,b2,bc,bc2,plr,dat.enemies[random(2)+(4*plr.map)],dat.lists,dat.sprites,dat.moves);
            plr.x:=plr.xBuffer;
            plr.y:=plr.yBuffer;
          end;
          while keypressed do k:=getkeyevent; // clearing keybuffer

          

      Until (quit or (plr.hp<=0));// double esc using q instead
      menu:=true;
    end;
  until (quit);
       
      //controlls end
    //game loop end

  //game end routine
  DoneKeyBoard;         //deactivate keyboard driver
  cursoron;             // activate cursor
  textcolor(lightgray); //restore default color
end.
