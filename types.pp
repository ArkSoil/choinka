unit types;
{$mode objfpc}
interface

type
	//engine
	list = record
		size: byte;
		field: array[0..4] of string[27];
	end;
	map = record
		field: array[0..31] of array[0..32] of char;
		height, width, startX, startY, startD: byte
	end;
	charBuffer = array [1..80] of array[1..23] of char;
	colorBuffer = array [1..80] of array[1..23] of byte;
	visionBuffer = array[0..11] of char;
	log = array[0..5] of string[50];


	//gameplay

	event = record
		x,y,eventType:Byte;
	end;

	sprite = record
		name: string[20];
		scr: array[0..25] of array[0..16] of char;
		clrMap: array[0..50] of array[0..25] of byte;
		height: byte;
		width: byte;
	end;

	move = record 
		name: string[27];
		cost,atc: byte;
	end;
	enemy = record
		name:string[20];
		spr,atc:byte;
		hp, xpDrop: integer;
	end;

	player = record
		name: string[20];
		x,y,xBuffer,yBuffer,map,str,vit,int,lvl:byte;
		hp,xp,mp,hpMax,xpMax,mpMax: integer;
		moves: array[0..4] of move;
		healhp, healmp, d:byte;
		itemState: string[8];
  	eventState: string[10];
	end;

	mapA    = array[0..2] 		 of types.map;
	spriteA = array[0..8] 		 of types.sprite;
	moveA   = array[0..4] 		 of types.move;
	enemyA  = array[0..11] 		 of types.enemy;
	listA   = array[0..4] 		 of types.list;
	eventA  = array[0..2,0..9] of types.event;
	eventM	= array[0..9]			 of types.event;

	data = record 
		maps:		 mapA;
		sprites: spriteA;
		moves:	 moveA;
		enemies: enemyA;
		lists:	 listA;
		events:	 eventA;
	end;

function mapToVision(map1:map;x,y,d:byte):visionBuffer;

implementation
//a - 10
//b - 11
//a879b
//53046
// 1X2
function mapToVision(map1:map;x,y,d:byte):visionBuffer;
	var
		vision:visionBuffer;
	begin
		case d of
			0: begin
					vision[0]:=map1.field[y-1][x];
					vision[1]:=map1.field[y][x-1];
					vision[2]:=map1.field[y][x+1];
					vision[3]:=map1.field[y-1][x-1];
					vision[4]:=map1.field[y-1][x+1];
					if x-2 > 0 then
						vision[5]:=map1.field[y-1][x-2]
					else
						vision[5]:='1';
					if x+2 < map1.width then
						vision[6]:=map1.field[y-1][x+2]
					else
						vision[6]:='1';
//---------------------------------------------------
					if y-2 > 0 then
						begin
							vision[7]:=map1.field[y-2][x];
							vision[8]:=map1.field[y-2][x-1];
							vision[9]:=map1.field[y-2][x+1];
							if x-2 > 0 then
								vision[10]:=map1.field[y-2][x-2]
							else
								vision[10]:='1';
							if x+2 < map1.width then
								vision[11]:=map1.field[y-2][x+2]
							else
								vision[11]:='1';
						end
					else
						begin
							vision[7]:='1';
							vision[8]:='1';
							vision[9]:='1';
							vision[10]:='1';
							vision[11]:='1';
						end;
					end;

			1: begin
					vision[0]:=map1.field[y][x+1];
					vision[1]:=map1.field[y-1][x];
					vision[2]:=map1.field[y+1][x];
					vision[3]:=map1.field[y-1][x+1];
					vision[4]:=map1.field[y+1][x+1];
					if y-2 > 0 then
						vision[5]:=map1.field[y-2][x+1]
					else
						vision[5]:='1';
					if y+2 < map1.height then
						vision[6]:=map1.field[y+2][x+1]
					else
						vision[6]:='1';
//---------------------------------------------------
					if x+2 < map1.width then
					begin
						vision[7]:=map1.field[y][x+2];
						vision[8]:=map1.field[y-1][x+2];
						vision[9]:=map1.field[y+1][x+2];
						if y-2 > 0 then
							vision[10]:=map1.field[y-2][x+2]
						else
							vision[10]:='1';
						if y+2 < map1.height then
							vision[11]:=map1.field[y+2][x+2]
						else
							vision[11]:='1';
					end
				else
					begin
						vision[7]:='1';
						vision[8]:='1';
						vision[9]:='1';
						vision[10]:='1';
						vision[11]:='1';
					end;
					end;

			2: begin
					vision[0]:=map1.field[y+1][x];
					vision[1]:=map1.field[y][x+1];
					vision[2]:=map1.field[y][x-1];
					vision[3]:=map1.field[y+1][x+1];
					vision[4]:=map1.field[y+1][x-1];
					if x+2 < map1.width then
						vision[5]:=map1.field[y+1][x+2]
					else
						vision[5]:='1';
					if x-2 > 0 then
						vision[6]:=map1.field[y+1][x-2]
					else
						vision[6]:='1';
//---------------------------------------------------
					if y+2  < map1.height then
						begin
							vision[7]:=map1.field[y+2][x];
							vision[8]:=map1.field[y+2][x+1];
							vision[9]:=map1.field[y+2][x-1];
							if x-2 > 0 then
								vision[10]:=map1.field[y+2][x+2]
							else
								vision[10]:='1';
							if x+2 < map1.width then
								vision[11]:=map1.field[y+2][x-2]
							else
								vision[11]:='1';
						end
					else
						begin
							vision[7]:='1';
							vision[8]:='1';
							vision[9]:='1';
							vision[10]:='1';
							vision[11]:='1';
						end;
					end;

	3: begin
			vision[0]:=map1.field[y][x-1];
			vision[1]:=map1.field[y+1][x];
			vision[2]:=map1.field[y-1][x];
			vision[3]:=map1.field[y+1][x-1];
			vision[4]:=map1.field[y-1][x-1];
				if y+2 < map1.height then
			vision[5]:=map1.field[y+2][x-1]
				else
			vision[5]:='1';
				if y-2 > 0 then
			vision[6]:=map1.field[y-2][x-1]
				else
			vision[6]:='1';
//---------------------------------------------------
			if x-2 > 0 then
				begin
					vision[7]:=map1.field[y][x-2];
					vision[8]:=map1.field[y+1][x-2];
					vision[9]:=map1.field[y-1][x-2];
					if y-2 > 0 then
						vision[10]:=map1.field[y+2][x-2]
					else
						vision[10]:='1';
					if y+2 < map1.height then
						vision[11]:=map1.field[y-2][x-2]
					else
						vision[11]:='1';
				end
			else
				begin
					vision[7]:='1';
					vision[8]:='1';
					vision[9]:='1';
					vision[10]:='1';
					vision[11]:='1';
				end;
			end;
		end;
		mapToVision:=vision;
	end;

end.