program convert;//converting data into binary
{$mode objfpc}
uses types,crt,sysutils;
		
var
		lists:      types.listA;
		maps:       types.mapA;
		sprite1:    types.spriteA;
		move1:      types.moveA;
		enemies:    types.enemyA;
		events:			types.eventA;
		charClrMap: array[0..50] of array[0..25] of char;
		file1: text;
		i,j,x,z: integer;
		outfileS: file of types.spriteA;
		outfileM: file of types.moveA;
		outfileF: file of types.mapA;
		outfileE: file of types.enemyA;
		outfileL: file of types.listA;
		outfileV: file of types.eventA;
		outfileD: file of types.data;
		dat: data;
begin
//sprites convert
	Assign(file1, 'sprites');
	Reset(file1);
	readln(file1, z);
	for x:=0 to z-1 do
		begin
			readln(file1, sprite1[x].name);
			readln(file1, sprite1[x].height);
			readln(file1, sprite1[x].width);
			for j:=0 to sprite1[x].height-1 do
				for i:=0 to sprite1[x].width do
						Read(file1, sprite1[x].scr[i][j]);
			for j:=0 to sprite1[x].height-1 do
				for i:=0 to sprite1[x].width do
					begin
						Read(file1, charClrMap[i][j]);
						case charClrMap[i][j] of
							'r':sprite1[x].clrMap[i][j]:=28;
							'g':sprite1[x].clrMap[i][j]:=26;
							'b':sprite1[x].clrMap[i][j]:=25;
							'2':sprite1[x].clrMap[i][j]:=2;
							'6':sprite1[x].clrMap[i][j]:=14;
							'4':sprite1[x].clrMap[i][j]:=6;
							'5':sprite1[x].clrMap[i][j]:=12;
							'1':sprite1[x].clrMap[i][j]:=1;
							' ':sprite1[x].clrMap[i][j]:=255;
							'0':sprite1[x].clrMap[i][j]:=0;
							'7':sprite1[x].clrMap[i][j]:=7;
							'3':sprite1[x].clrMap[i][j]:=15;
							'8':sprite1[x].clrMap[i][j]:=8;
						end;
					end;
		end;
	Close(file1);
	Assign(outfileS,concat('splitdata/sprite.dat'));
	Rewrite(outfileS);
	write(outfileS, sprite1);
	close(outfileS);

//move convert
	Assign(file1, 'moves');
	Reset(file1);
	readln(file1, z);
	for x:=0 to z-1 do
		begin
			readln(file1, move1[x].name);
			readln(file1, move1[x].cost);
			readln(file1, move1[x].atc);	
		end;
	Close(file1);
	Assign(outfileM,concat('splitdata/move.dat'));
	Rewrite(outfileM);
	write(outfileM, move1);
	close(outfileM);
//map convert
	Assign(file1, 'maps');
	Reset(file1);
	readln(file1, z);
	for x:=0 to z-1 do
		begin
			readln(file1, maps[x].width);
			readln(file1, maps[x].height);
			readln(file1, maps[x].startX);
			readln(file1, maps[x].starty);
			readln(file1, maps[x].startD);
			for j:=0 to maps[x].height-1 do
				for i:=0 to maps[x].width do
						Read(file1, maps[x].field[j][i]);			
		end;
	Close(file1);
	Assign(outfileF,concat('splitdata/map.dat'));
	Rewrite(outfileF);
	write(outfileF, maps);
	close(outfileF);

//enemy convert
	Assign(file1, 'enemies');
	Reset(file1);
	readln(file1, z);
	for x:=0 to z-1 do
		begin
			readln(file1, enemies[x].name);
			readln(file1, enemies[x].spr);
			readln(file1, enemies[x].atc);
			readln(file1, enemies[x].hp);
			readln(file1, enemies[x].xpDrop);
		end;
	Close(file1);
	Assign(outfileE,concat('splitdata/enemy.dat'));
	Rewrite(outfileE);
	write(outfileE, enemies);
	close(outfileE);
//list convert
	Assign(file1, 'lists');
	Reset(file1);
	readln(file1, z);
	for i:=0 to z-1 do
		begin
			readln(file1,lists[i].size);
			for j:=0 to lists[i].size-1 do
				readln(file1, lists[i].field[j]);
		end;
	Close(file1);
	Assign(outfileL,concat('splitdata/list.dat'));
	Rewrite(outfileL);
	write(outfileL, lists);
	close(outfileL);
//event convert
	Assign(file1, 'events');
	Reset(file1);
	readln(file1, z);
	for i:=0 to z-1 do
		begin
			readln(file1, x);
			for j:=0 to x-1 do
				begin
					readln(file1, events[i][j].eventType);
					readln(file1, events[i][j].x);
					readln(file1, events[i][j].y);
				end;
		end;
	Close(file1);
	Assign(outfileV,concat('splitdata/event.dat'));
	Rewrite(outfileV);
	write(outfileV, events);
	close(outfileV);
//data convert
	dat.maps:=maps;
	dat.sprites:=sprite1;
	dat.moves:=	move1;
	dat.enemies:=enemies;
	dat.lists:=	 lists;
	dat.events:=	events;

	Assign(outfileD,concat('data.dat'));
	Rewrite(outfileD);
	write(outfileD, dat);
	close(outfileD);
end.